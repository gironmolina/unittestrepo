﻿using System.Linq;

namespace DefenseGame
{
    public class Path
    {
        private readonly MapLocation[] path;

        public Path(MapLocation[] mapLocation)
        {
            path = mapLocation;
        }

        public bool IsOnPatch(MapLocation mapLocation) => path.Contains(mapLocation);
    }
}